Illumina BaseSpace/BaseMount image

[BaseSpace](https://basespace.illumina.com)

[BaseMount](https://basemount.basespace.illumina.com/)

1. Start container.
```bash
    docker run -it \
        -v some-host-directory:/volume \
        --cap-add=SYS_ADMIN \
        --security-opt apparmor:unconfined \
        --security-opt seccomp=unconfined \
        --device /dev/fuse \
        huanjason/basespace
```

2. Open link that is displayed in browser and grant permission.

3. Inside the container, copy the files you want to download from /basemount/ to /volume/.

4. The files will be in the **some-host-directory** mount point on the host computer.
